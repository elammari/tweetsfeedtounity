﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTest : MonoBehaviour 
{

	public string key, secret, accesstoken;
 	[SerializeField]
 	Twitter.TwitterUser newUser;
	[SerializeField]
 	Twitter.Tweet[] tweets;

	// Use this for initialization
	void Start () {

		accesstoken = Twitter.API.GetTwitterAccessToken(key,secret);

		newUser = Twitter.API.GetProfileInfo("@realDonaldTrump",accesstoken,false);

		tweets = Twitter.API.SearchForTweets("#USA",accesstoken,2,Twitter.API.SearchResultType.recent);

		//Twitter.API.SearchForTweets(QueryInput.text,AccessToken,(int)tweetNumberSliderQuery.value,Twitter.API.SearchResultType.popular);

//		Debug.Log ("========================"+newUser.ToString());

		//Debug.Log(tweets.Length + " tweets found");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
